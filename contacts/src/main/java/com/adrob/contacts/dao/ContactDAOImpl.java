package com.adrob.contacts.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.SessionFactory;
import com.adrob.contacts.model.Contact;
import com.adrob.commons.presistence.dao.*;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ContactDAOImpl implements ContactDAO {
	@Inject
	private SessionFactory sessionFactory;

	public void addContact(Contact contact) {
		sessionFactory.getCurrentSession().save(contact);
	}

	public List<Contact> listContact() {

		return sessionFactory.getCurrentSession().createQuery("from Contact")
				.list();
	}

	public void removeContact(Integer id) {
		Contact contact = (Contact) sessionFactory.getCurrentSession().load(
				Contact.class, id);
		if (null != contact) {
			sessionFactory.getCurrentSession().delete(contact);
			sessionFactory.getCurrentSession().flush();
		}

	}
	
	public Contact findContactById(Integer id){
		Contact contact = (Contact) sessionFactory.getCurrentSession().load(
				Contact.class, id);
		return contact;
	}
	
	public void updateContact(Contact contact) {
		sessionFactory.getCurrentSession().saveOrUpdate(contact);
		sessionFactory.getCurrentSession().flush();

	}

}
