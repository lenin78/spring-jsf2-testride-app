package com.adrob.contacts.dao;

import java.util.List;

import com.adrob.contacts.model.Contact;

public interface ContactDAO{
	public void addContact(Contact contact);
    public List<Contact> listContact();
    public void removeContact(Integer id);
    public void updateContact(Contact contact);
    public Contact findContactById(Integer id);
}
