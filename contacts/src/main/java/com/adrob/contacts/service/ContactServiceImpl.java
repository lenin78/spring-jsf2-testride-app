package com.adrob.contacts.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Named;
import com.adrob.contacts.dao.ContactDAO;
import com.adrob.contacts.model.Contact;
import com.adrob.security.dao.UserDetailsDAO;
import com.adrob.security.service.CredentialsService;
import com.adrob.security.service.UserDetailsServiceImpl;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
public class ContactServiceImpl implements ContactService {

	@Inject
	private ContactDAO contactDAO;
	@Inject
	UserDetailsDAO userDetailsDAO;
	@Inject
	private CredentialsService credentialsService;

	public CredentialsService getCredentialsService() {
		return credentialsService;
	}

	public void setCredentialsService(CredentialsService credentialsService) {
		this.credentialsService = credentialsService;
	}

	@Transactional
	public void addContact(Contact contact) {
		System.out.println("service adding contact");

		String name = credentialsService.getUserName();
		int id = userDetailsDAO.findByName(name).getId();
		contact.setOwnerId(id);
		contactDAO.addContact(contact);
	}

	@Transactional
	public List<Contact> listContact() {
		System.out.println("listing contacts");

		String name = credentialsService.getUserName();
		int id = userDetailsDAO.findByName(name).getId();
		List<Contact> listByUser = new ArrayList<Contact>();
		for (Contact contact : contactDAO.listContact()) {
			if (contact.getOwnerId() == id) {
				listByUser.add(contact);
			}
		}
		return listByUser;
	}

	@Transactional
	public void removeContact(Integer id) {
		contactDAO.removeContact(id);
	}

	@Transactional
	public Contact findContactById(Integer id) {
		return contactDAO.findContactById(id);
	}

	@Transactional
	public void updateContact(Contact contact) {
		contactDAO.updateContact(contact);
	}

	@Transactional
	public void updateBulkContacts(List<Contact> contactList) {
		for (Contact contact : contactList) {
			contactDAO.updateContact(contact);
		}

	}
}
