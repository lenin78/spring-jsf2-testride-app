package com.adrob.contacts.service;

import java.util.List;

import com.adrob.contacts.model.Contact;
import com.adrob.security.service.CredentialsService;


 
public interface ContactService {
    public void updateBulkContacts(List<Contact> contactList); 
    public void addContact(Contact contact);
    public List<Contact> listContact();
    public void removeContact(Integer id);
    public void updateContact(Contact contact);
    //public void setCredentialsService(CredentialsService credentialsService);
    public Contact findContactById(Integer id);
}