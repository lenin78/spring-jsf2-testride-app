package com.adrob.contacts.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;




@Entity
@Table(name = "CONTACTS")
public class Contact {
	 @Id
	    @Column(name="ID")
	 	@SequenceGenerator(name="alias_for_my_sequence_in_oracle", sequenceName="contacts_seq")
	 	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="alias_for_my_sequence_in_oracle")
	    private Integer id;
	     
	    @Column(name="FIRSTNAME")
	    private String firstName;
	 
	    @Column(name="LASTNAME")
	    private String lastName;
	 
	    @Column(name="EMAIL")
	    private String email;
	     
	    @Column(name="TELEPHONE")
	    private String telephone;
	    
	    @Column(name="OWNER_ID")
	    private Integer ownerId;
	    
	    @Override
	    public int hashCode() {
	        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
	            // if deriving: appendSuper(super.hashCode()).
	            append(firstName).
	            append(lastName).
	            append(email).
	            append(telephone).
	            append(id).
	            append(ownerId).
	            toHashCode();
	    }
	    @Override
	    public boolean equals(Object that){
	    	if(that==null)
	    		return false;
	    	if(that==this)
	    		return true;
	    	if(!(that instanceof Contact))
	    		return false;
	    	Contact rhs = (Contact) that;
	    	   return new EqualsBuilder()
	    	                 .append(id, rhs.id)
	    	                 .append(ownerId, rhs.ownerId)
	    	                 .append(firstName, rhs.firstName)
	    	                 .append(lastName, rhs.lastName)
	    	                 .append(email, rhs.email)
	    	                 .append(telephone, rhs.telephone)
	    	                 .isEquals();
	    }
	     
	    public Integer getOwnerId() {
			return ownerId;
		}
		public void setOwnerId(Integer ownerId) {
			this.ownerId = ownerId;
		}
		public String getEmail() {
	        return email;
	    }
	    public String getTelephone() {
	       
                return telephone;
	    }
	    public void setEmail(String email) {
	        this.email = email;
	    }
	    public void setTelephone(String telephone) {
	       
                this.telephone = telephone;
	    }
	    public String getFirstName() {
	        return firstName;
	    }
	    public String getLastName() {
	        return lastName;
	    }
	    public void setFirstName(String firstName) {
	    
	        this.firstName = firstName;
	    }
	    public void setLastName(String lastName) {
	        this.lastName = lastName;
	    }
	    public Integer getId() {
	        return id;
	    }
	    public void setId(Integer id) {
	        this.id = id;
	    }
}
