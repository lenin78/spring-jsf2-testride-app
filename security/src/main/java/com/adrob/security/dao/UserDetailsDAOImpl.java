package com.adrob.security.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.adrob.security.model.UserEntity;

@Repository
public class UserDetailsDAOImpl implements UserDetailsDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public UserEntity findByName(String name) {

		try{
		List<UserEntity> users = sessionFactory.getCurrentSession().createQuery(
				"from UserEntity where username='"+name+"'").list();
		System.out.println(users.get(0).getPassword());
		return users.get(0);
		}catch(Exception e)
		{
			e.printStackTrace();
		return null;
		}
		
	}

}
