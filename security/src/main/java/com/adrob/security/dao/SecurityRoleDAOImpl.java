package com.adrob.security.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.adrob.security.model.SecurityRoleEntity;

@Repository
public class SecurityRoleDAOImpl implements SecurityRoleDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public String getRoleNameForID(Integer id) {
		// TODO Auto-generated method stub
		try {
			SecurityRoleEntity result = (SecurityRoleEntity)sessionFactory
					.getCurrentSession()
					.createQuery("from SecurityRoleEntity where id=" + id)
					.uniqueResult();
			return result.getName();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
