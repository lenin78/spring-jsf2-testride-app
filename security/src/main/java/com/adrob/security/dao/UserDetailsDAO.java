package com.adrob.security.dao;



import com.adrob.security.model.UserEntity;

public interface UserDetailsDAO {
UserEntity findByName(String name);
}
