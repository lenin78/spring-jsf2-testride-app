package com.adrob.security.dao;

public interface SecurityRoleDAO {

	public String getRoleNameForID(Integer id);
}
