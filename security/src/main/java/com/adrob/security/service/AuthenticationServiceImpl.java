/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adrob.security.service;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author adam
 */
@Named
public class AuthenticationServiceImpl implements AuthenticationService{

    @Inject
    AuthenticationManager authenticationManager;
    
    @Override
    public void Authenticate(String login, String password) throws AuthenticationException {
            
                        
            String md5Password = DigestUtils.md5Hex(password);
            
            System.out.println("login bean md5 password: " + md5Password);
            Authentication request = new UsernamePasswordAuthenticationToken(login, md5Password);
            Authentication result = authenticationManager.authenticate(request);
            SecurityContextHolder.getContext().setAuthentication(result);
            
            
           
                  
    }

    @Override
    public boolean unAuthenticate() {
        try{
        SecurityContextHolder.clearContext();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        externalContext.invalidateSession();
        return true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }
    
}
