/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adrob.security.service;

import org.springframework.security.core.AuthenticationException;



/**
 *
 * @author adam
 */

public interface AuthenticationService {
    public void Authenticate(String login, String password) throws AuthenticationException;
    public boolean unAuthenticate();
}
