package com.adrob.security.service;

public interface CredentialsService {

	public String getUserName();
}
