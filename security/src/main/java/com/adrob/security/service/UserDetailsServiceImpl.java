package com.adrob.security.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adrob.security.dao.UserDetailsDAO;
import com.adrob.security.model.UserEntity;
import javax.inject.Inject;
import javax.inject.Named;

@Named("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
	private static final Logger logger = LoggerFactory
			.getLogger(UserDetailsServiceImpl.class);
	@Inject
	private UserDetailsDAO dao;
	@Inject
	private UserEntityAssemblerService assembler;

	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		logger.info("Loggin attempt by:" + username);
		System.out.println(username);
		UserDetails userDetails = null;
		UserEntity userEntity = dao.findByName(username);
		if (userEntity == null)
			throw new UsernameNotFoundException("user not found");

		return assembler.buildUserFromUserEntity(userEntity);
	}
}
