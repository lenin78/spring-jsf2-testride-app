package com.adrob.security.service;

import org.springframework.security.core.userdetails.User;

import com.adrob.security.model.UserEntity;

public interface UserEntityAssemblerService {
User buildUserFromUserEntity(UserEntity userEntity);
}
