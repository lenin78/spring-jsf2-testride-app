package com.adrob.security.service;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adrob.security.dao.SecurityRoleDAO;
import com.adrob.security.model.UserEntity;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class UserEntityAssemblerServiceImpl implements UserEntityAssemblerService{

	@Inject
	private SecurityRoleDAO dao;

	@Transactional(readOnly = true)
	public User buildUserFromUserEntity(UserEntity userEntity) {

		String username = userEntity.getName();
		String password = StringUtils.lowerCase(userEntity.getPassword());
		System.out.println(password);
		Integer roleId = userEntity.getRole();
		boolean enabled = userEntity.getActive() == 1 ? true : false;
		boolean accountNonExpired = userEntity.getActive() == 1 ? true : false;
		boolean credentialsNonExpired = userEntity.getActive() == 1 ? true
				: false;
		boolean accountNonLocked = userEntity.getActive() == 1 ? true : false;

		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		// for (SecurityRoleEntity role : userEntity.getRoles()) {
		authorities
				.add(new SimpleGrantedAuthority(dao.getRoleNameForID(roleId)));
		// }

		User user = new User(username, password, enabled, accountNonExpired,
				credentialsNonExpired, accountNonLocked, authorities);
		return user;
	}
}
