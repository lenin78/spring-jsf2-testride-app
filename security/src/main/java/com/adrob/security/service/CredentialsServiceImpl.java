package com.adrob.security.service;

import javax.inject.Named;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Named
public class CredentialsServiceImpl implements CredentialsService {

	@Override
	public String getUserName() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth == null) {
			return null;
		}
		return auth.getName();
	}

}
