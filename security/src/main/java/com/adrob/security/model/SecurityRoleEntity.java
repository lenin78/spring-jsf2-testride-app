package com.adrob.security.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user$roles")
public class SecurityRoleEntity {
	
	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="role")
	private String name;
	
	@Column(name="internal_privileges")
	private String privileges;
	
	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrivileges() {
		return privileges;
	}
	public void setPrivileges(String privileges) {
		this.privileges = privileges;
	}

}
