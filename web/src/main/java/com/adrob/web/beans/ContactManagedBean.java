package com.adrob.web.beans;

import com.adrob.contacts.model.Contact;
import com.adrob.contacts.service.ContactService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;

import org.primefaces.event.CellEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

@Named
@Scope("session")
public class ContactManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    @Inject
    private ContactService contactService;
    private List<Contact> contactList;
    private String firstName;
    private String lastName;
    private String email;
    private String telephone;
    private Contact selectedContact;
    private Contact[] selectedContacts;

    public void delete() {
        System.out.println("delete");
        try{
        System.out.println(getSelectedContact().getFirstName());
        System.out.println(getContactList().remove(getSelectedContact()));
        getContactService().removeContact(getSelectedContact().getId());
        }catch(Exception e){
            e.printStackTrace();
            
        }
    }

    public void onRowEdit(RowEditEvent event) {

        Contact contact = (Contact) event.getObject();
        
        FacesMessage msg = new FacesMessage("Car Edited", contact.getFirstName());
        getContactService().updateContact(contact);
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public void onRowSelect(SelectEvent event) {
        try{
        System.out.println("row selected..." + ((Contact)event.getObject()).getFirstName());
        //setSelectedContact((Contact)event.getObject());
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void onCellEdit(CellEditEvent event) {
        try{
        //getContactService().updateBulkContacts(getContactList());
        Object newValue = event.getNewValue();
        Object oldValue = event.getOldValue();
        System.out.println((String)newValue+"@@@@@@@@@@@@@@@@@@@@@");
        System.out.println((String)oldValue+"@@@@@@@@@@@@@@@@@@@@@");
        int itemIndex = event.getRowIndex();
        //Contact contact = contactList.get(itemIndex);
        
        //String columnName = event.getColumn().getHeaderText();
        
        if(newValue!=null && !newValue.equals(oldValue)){    
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO ,"Contact Edited:" , (String)newValue);
        //getContactService().updateContact(contact);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void add() {
        try {
            System.out.println("adding");
            Contact contact = new Contact();
            contact.setEmail(getEmail());
            contact.setFirstName(getFirstName());
            contact.setLastName(getLastName());
            contact.setTelephone(getTelephone());
            getContactService().addContact(contact);
            getContactList().add(contact);
            System.out.println("added");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    public ContactService getContactService() {
        return contactService;
    }

    public void setContactService(ContactService contactService) {
        this.contactService = contactService;
    }

    @PostConstruct
    public List<Contact> getContactList() {
        if (contactList == null) {
            
            contactList = getContactService().listContact();
        }
        
        return contactList;
    }

    public void setContactList(List<Contact> contactList) {
        System.out.println("set list contacts from managed bean###");
        this.contactList = contactList;
    }

    public String getFirstName() {
        try{
        return firstName;
        }catch(Exception e){
            e.printStackTrace();
        }
        return "failed";
    }

    /**
     * @param name the name to set
     */
    public void setFirstName(String firstName) {
        try{
        this.firstName = firstName;
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @return the selectedContact
     */
    public Contact getSelectedContact() {
        return selectedContact;
    }
    
    
    /**
     * @param selectedContact the selectedContact to set
     */
    public void setSelectedContact(Contact selectedContact) {
        
        
        this.selectedContact = selectedContact;
    }

    /**
     * @return the selectedContacts
     */
    public Contact[] getSelectedContacts() {
        return selectedContacts;
    }

    /**
     * @param selectedContacts the selectedContacts to set
     */
    public void setSelectedContacts(Contact[] selectedContacts) {
        this.selectedContacts = selectedContacts;
    }
}
