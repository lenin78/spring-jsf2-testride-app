/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adrob.web.beans;

import javax.inject.Named;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author adam
 */
@Named
@Scope("application")
public class NavigatorBean {
    
    public String contacts()
    {
        return "contacts";
    }
    
}
