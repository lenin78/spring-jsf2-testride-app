/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adrob.web.beans;

import com.adrob.security.service.AuthenticationService;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author adam
 */
@Named
@Scope("session")
public class LoginBean {

    private String loginName;
    private String password;
    @Inject
    private AuthenticationService authenticationService;

    public String login() {

        String login = getLoginName();
        String password = getPassword();
        AuthenticationService authenticationService = getAuthenticationService();
        try {
            authenticationService.Authenticate(login, password);
            return "success";
        } catch (AuthenticationException e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO ,"login error", e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            e.printStackTrace();
            return "loggedout";
        }




    }

    public String logout() {
        getAuthenticationService().unAuthenticate();
        return "loggedout";
    }

    /**
     * @return the login
     */
    public String getLoginName() {
        return loginName;
    }

    /**
     * @param login the login to set
     */
    public void setLoginName(String login) {
        this.loginName = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the authenticationService
     */
    public AuthenticationService getAuthenticationService() {
        return authenticationService;
    }

    /**
     * @param authenticationService the authenticationService to set
     */
    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }
}
